//console.log("Hello World");

/*
	3. Create a variable getCube and use the exponent operator to compute the cube of a number. (A cube is any number raised to 3)
	4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
	
	5. Create a variable address with a value of an array containing details of an address.
	6. Destructure the array and print out a message with the full address using Template Literals.

	7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
	8. Destructure the object and print out a message with the details of the animal using Template Literals.

	9. Create an array of numbers.
	10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.

	11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.

	12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
	13. Create/instantiate a new object from the class Dog and console log the object.
*/

let getCube = 2;
console.log(`The cube of ${getCube} is ${getCube ** 3}`);

let address = ["258 Washington Ave NW", "California 90011"];
const [street, country] = address;
console.log(`I live at ${street}, ${country}`);

let animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: 1075,
	measurementFt: 20,
	measurementIn: 3
};
const{name, species, weight, measurementFt, measurementIn} = animal;
console.log(`${name} was a ${species}. He weighed ${weight} kgs with a measurement of ${measurementFt} ft ${measurementIn} in.`);

let numbers = [1, 2, 3, 4, 5];
numbers.forEach((number) => console.log(number));

let reduceNumber = numbers.reduce((acc, curr) => acc+curr);
console.log(reduceNumber);

class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}
let pet = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(pet);